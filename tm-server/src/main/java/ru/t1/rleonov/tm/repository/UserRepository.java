package ru.t1.rleonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.IUserRepository;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role
    ) {
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return models
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return models
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return models
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return models
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
